import { Ingredient } from "app/models/ingredient.model";

export class Product {
  public id: string;
  public name: string;
  public thumb: string;
  public description: string;
  public price: number;

  public updateFrom(src: Product): void {
    this.id = src.id;
    this.name = src.name;
    this.thumb = src.thumb;
    this.description = src.description;
    this.price = src.price;
  }
}
