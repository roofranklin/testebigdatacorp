## Teste desenvolvido para a Big Data Corp referente a habilidades em Front-End

O carrinho de compras foi desenvolvido utilizando os seguintes recursos:

1. Gulp
2. Bootstrap 4.0 com SASS
3. Angular 4

---

## Instalando o repositório localmente

Basta seguir os passos abaixo:

1. Abra o terminal no diretório desejado e faça o comando git clone https://roofranklin@bitbucket.org/roofranklin/testebigdatacorp.git para baixar o repositório
2. cd testeBigDataCorp
3. npm install
4. npm run start
5. Pronto! O projeto estará rodando em http://localhost:4200